const express = require('express')
const axios = require('axios')
const app = express()
const port = 3000

app.get('/', (req, res) => {
    res.send('Hello, TDEM')
})

app.get('/help', (req, res) => {
    res.send('Help page')
})

app.get('/about', (req, res) => {
    res.send('<h1>About page<h1>')
})

app.get('/weather', (req, res) => {
    axios.get('http://api.weatherstack.com/current?access_key=6350a9db621dc902fd0d4e0633bfd464&query=13.753960,100.502243')
        .then(response => {
            const data = {
                temperature: response.data.current.temperature + '°C',
                location: response.data.location.name,
                localtime: response.data.location.localtime,
            }
            res.send(data);
        })
        .catch(error => {
            console.error(error);
            res.status(500).send('Error occurred while fetching weather data');
        });
});

app.listen(port, () => {
    console.info(`Server is running in ${port} mode on port ${port}`)
})